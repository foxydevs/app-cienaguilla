import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController, AlertController } from '@ionic/angular';
import { OrdenService } from 'src/app/_service/orden.service';
import { path } from 'src/app/config.module';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalMiCompraComponent } from '../mi-compra/modal-mi-compra/modal-mi-compra.component';
import { VentaService } from 'src/app/_service/venta.service';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.page.html',
  styleUrls: ['./pedido.page.scss'],
})
export class PedidoPage implements OnInit {
  //PROPIEDADES 
  public ventas:any[] = [];
  public idProduct:any;
  public idUser:any = path.id;
  public selectItem:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  moneda = localStorage.getItem('currentCurrency');

  constructor(
    private router: Router,
    private location: Location,
    public modal: ModalController,
    private modalController: ModalController,
    public ordersService: OrdenService,
    public ventaService: VentaService,
    private notificacionService: NotificacionService,
    private alertController: AlertController
  ) {
    this.selectItem = 'pagados';
    this.getAll(this.idUser, 1);
  }

  ngOnInit() {
  }

  public getAll(id:any, state:any) {
    this.notificacionService.alertLoading('Cargando...', 10000);
    this.ventaService.getOrdersByUserState(id, state)
    .subscribe(response => {
      console.log(response)
      this.ventas = [];
      this.ventas = response;
      this.ventas.reverse();
      this.notificacionService.dismiss();
    }, error => {
      console.clear
      this.notificacionService.dismiss();
    })
  }

  async cancel(id:string) {
    let order = {
      opcion: "0",
      entrega: null,
      id: id
    }
    const alert = await this.alertController.create({
      header: 'Confirmación',
      message: '¿Deseas cancelar el pedido?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            //console.log('Confirm Okay');
            this.selectItem = 'cancelados';
            this.update(order)
          }
        }
      ]
    });

    await alert.present();
  }

  async accept(id:string) {
    let order = {
      state: '2',
      id: id
    }
    const alert = await this.alertController.create({
      header: 'Confirmación',
      message: '¿Desea envíar el pedido?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            //console.log('Confirm Okay');
            this.selectItem = 'cancelados';
            this.update(order)
          }
        }
      ]
    });

    await alert.present();
  }


  //REFRESCAR
  doRefresh(event) {

    setTimeout(() => {
      if(this.selectItem == 'pagados') {
        this.getAll(this.idUser, 1);
      } else if(this.selectItem == 'pendientes') {
        this.getAll(this.idUser, 3);
      } else if(this.selectItem == 'enviados') {
        this.getAll(this.idUser, 4);
      } else if(this.selectItem == 'entregados') {
        this.getAll(this.idUser, 5);
      } else if(this.selectItem == 'cancelados') {
        this.getAll(this.idUser, 0);
      } else if(this.selectItem == 'deposito') {
        this.getAll(this.idUser, 2);
      }
      event.target.complete();
    }, 2000);
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
    if(this.selectItem == 'pagados') {
      this.getAll(this.idUser, 1);
    } else if(this.selectItem == 'pendientes') {
      this.getAll(this.idUser, 3);
    } else if(this.selectItem == 'enviados') {
      this.getAll(this.idUser, 4);
    } else if(this.selectItem == 'entregados') {
      this.getAll(this.idUser, 5);
    } else if(this.selectItem == 'cancelados') {
      this.getAll(this.idUser, 0);
    } else if(this.selectItem == 'deposito') {
      this.getAll(this.idUser, 2);
    }
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.ventaService.updateEstado(formValue)
    .subscribe((res) => {
      this.notificacionService.alertMessage('Orden Actualizada', 'La orden fue actualizada exitosamente.');
      //console.log(res);
      if(this.selectItem == 'pagados') {
        this.getAll(this.idUser, 1);
      } else if(this.selectItem == 'pendientes') {
        this.getAll(this.idUser, 3);
      } else if(this.selectItem == 'enviados') {
        this.getAll(this.idUser, 4);
      } else if(this.selectItem == 'entregados') {
        this.getAll(this.idUser, 5);
      } else if(this.selectItem == 'cancelados') {
        this.getAll(this.idUser, 0);
      } else if(this.selectItem == 'deposito') {
        this.getAll(this.idUser, 2);
      }
    }, (error) => {
      console.clear
    });
  }

  async presentModal(id:any) {
    const modal = await this.modalController.create({
      component: ModalMiCompraComponent,
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      if(this.selectItem == 'pagados') {
        this.getAll(this.idUser, 1);
      } else if(this.selectItem == 'pendientes') {
        this.getAll(this.idUser, 3);
      } else if(this.selectItem == 'enviados') {
        this.getAll(this.idUser, 4);
      } else if(this.selectItem == 'entregados') {
        this.getAll(this.idUser, 5);
      } else if(this.selectItem == 'cancelados') {
        this.getAll(this.idUser, 0);
      } else if(this.selectItem == 'deposito') {
        this.getAll(this.idUser, 2);
      }
    });

    return await modal.present();
  }

}
