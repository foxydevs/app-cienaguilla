import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalPerfilComponent } from './modal-perfil/modal-perfil.component';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  data = {
    picture: localStorage.getItem('currentPicture'),
    nombreCompleto: localStorage.getItem('currentFirstName') + " " + localStorage.getItem('currentLastName'),
    email: localStorage.getItem('currentEmail'),
    id: localStorage.getItem('currentId'),
  }
  myStyles = {
    "background-image": "url("+localStorage.getItem('currentPicture')+")"
  }
  administratorApp:string;
  
  constructor(
    private modalController: ModalController,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
    this.administratorApp = localStorage.getItem('currentType')
    this.data.id = localStorage.getItem('currentId');
    this.data.nombreCompleto = localStorage.getItem('currentFirstName') + " " + localStorage.getItem('currentLastName');
    this.data.email = localStorage.getItem('currentEmail');
    this.data.picture = localStorage.getItem('currentPicture');
    this.myStyles["background-image"] = "url("+localStorage.getItem('currentPicture')+")";
  }

  ionViewDidEnter() {
    this.administratorApp = localStorage.getItem('currentType')
    this.data.id = localStorage.getItem('currentId');
    this.data.nombreCompleto = localStorage.getItem('currentFirstName') + " " + localStorage.getItem('currentLastName');
    this.data.email = localStorage.getItem('currentEmail');
    this.data.picture = localStorage.getItem('currentPicture');
    this.myStyles["background-image"] = "url("+localStorage.getItem('currentPicture')+")";
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalPerfilComponent,
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      if(data.data) {
        this.data.email = localStorage.getItem('currentEmail');
        this.data.nombreCompleto = localStorage.getItem('currentFirstName') + " " + localStorage.getItem('currentLastName');
        this.data.picture = localStorage.getItem('currentPicture');
      }
    });

    return await modal.present();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  logOut() {
    this.goToRoute('tablogin/tablogin/login')
    localStorage.clear();
  }

}
