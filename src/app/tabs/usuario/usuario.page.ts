import { Component, OnInit } from '@angular/core';
import { path } from 'src/app/config.module';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss'],
})
export class UsuarioPage implements OnInit {
  search:string;
  table:any = [];
  usuarioID = path.id;

  constructor(
    private mainService: UsuarioService,
    private router: Router,
    private location: Location,
    private modalController: ModalController,
    private notificationService: NotificacionService,
  ) { }

  ngOnInit() {
    this.getAll();
  }
  
  getAll() {
    this.mainService.getClients(this.usuarioID)
    .subscribe((res) => {
      console.log(res)
      this.table = [];
      this.table = res;
      //console.log(res);
    }, (error) => {
      //console.log(error);
    })
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: "ModalDireccionComponent",
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      if(data.data) {
        this.getAll();
      }
    });

    return await modal.present();
  }

}
