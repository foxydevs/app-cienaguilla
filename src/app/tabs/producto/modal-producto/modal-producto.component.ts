import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/_service/producto.service';
import { NavParams, ModalController, AlertController } from '@ionic/angular';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { path } from 'src/app/config.module';
import { CategoriaService } from 'src/app/_service/categoria.service';
import { ModalPresentationComponent } from '../modal-presentation/modal-presentation.component';
import { PresentacionService } from 'src/app/_service/presentacion.service';

@Component({
  selector: 'app-modal-producto',
  templateUrl: './modal-producto.component.html',
  styleUrls: ['./modal-producto.component.scss'],
})
export class ModalProductoComponent implements OnInit {
  parameter:any;
  categories:any[];
  data:any;
  administratorApp:string;
  btnDisabled:boolean;
  selectItem:string = 'formulario';
  product = {
    name: '',
    id: 0,
    description : '',
    price: 0,
    quantity: 0,
    tiempo: 0,
    periodo: 0,
    membresia: 0,
    nivel: 1,
    cost : 0,
    user_created: path.id,
    presentacion: '',
    presentaciones_varias: [],
    presentacionNombre: '',
    category: '',
    digital: 0,
    ver: 0,
    opcion1: 0,
    libro: '',
    duracion: '0',
    idPic1: '',
    idPic2: '',
    idPic3: '',
    picture1: 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatars/QtZbn4Z3sLd0QCK7WcUB56tm4pGo88qNpi88TNMl.png',
    picture2: 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatars/QtZbn4Z3sLd0QCK7WcUB56tm4pGo88qNpi88TNMl.png',
    picture3: 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatars/QtZbn4Z3sLd0QCK7WcUB56tm4pGo88qNpi88TNMl.png',
    libros: 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatars/QtZbn4Z3sLd0QCK7WcUB56tm4pGo88qNpi88TNMl.png',
    picture: 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatars/QtZbn4Z3sLd0QCK7WcUB56tm4pGo88qNpi88TNMl.png'
  }
  constructor(
    private productoService: ProductoService,
    private thirdService: PresentacionService,
    private categoriaService: CategoriaService,
    private notificationService: NotificacionService,
    private alertController: AlertController,
    private navParams: NavParams,
    private modalController: ModalController
  ) { }

  getAllCategoria = (id:number) => {
    this.categoriaService.getByUser(id)
    .subscribe((res)=>{
      this.categories = [];
      this.categories = res;
    }, (error) => { 
    });
  }

  closeModal() {
    this.modalController.dismiss();
  }

  ngOnInit() {
    this.getAllCategoria(path.id)
    this.administratorApp = localStorage.getItem('currentType')
    this.parameter = this.navParams.get('value');
    setTimeout(() => {
      this.product.category = this.navParams.get('categoria');
    }, 3000);
    if(this.parameter) {
      this.getSingle(this.parameter)
    } else {

    }
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    if(this.product.name) {
      if(this.product.description) {
        if(this.parameter) {
          this.update(this.product)
        } else {
          this.create(this.product)
        }
      } else {
        this.notificationService.alertToast('La descripción es requerida.');
      }
    } else {
      this.notificationService.alertToast('El nombre es requerido.');
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.productoService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Producto Agregado', 'El Producto fue agregado exitosamente.');
      this.modalController.dismiss(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.productoService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Producto Actualizado', 'El Producto fue actualizado exitosamente.');
      this.modalController.dismiss(res);
      //console.log(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    this.productoService.getSingle(parameter)
    .subscribe(response => {
      console.log(response)
      this.data = response;
      this.product.id = response.id;
      this.product.name = response.name;
      this.product.description = response.description;
      this.product.price = response.price.toFixed(2);
      this.product.quantity = response.quantity;
      this.product.cost = response.cost.toFixed(2);
      this.product.category = response.category;
      this.product.picture = response.picture;
      this.product.tiempo = response.tiempo;
      this.product.periodo = response.periodo;
      this.product.membresia = response.membresia;
      this.product.nivel = response.nivel;
      this.product.digital = response.digital;
      this.product.libro = response.libro;
      this.product.ver = response.ver;
      this.product.presentacion = response.presentacion;
      this.product.presentaciones_varias = response.presentaciones_varias;
      this.product.duracion = response.duracion;
      this.product.opcion1 = response.opcion1;
      if(response.libro) {
        this.product.libros = 'https://developers.zamzar.com/assets/app/img/convert/pdf.png'
      }
      if(response.presentaciones) {
        this.product.presentacionNombre = response.presentaciones.name;
      }
      if(response.pictures) {
        if(response.pictures[0]) {
          this.product.picture1 = response.pictures[0].picture;
          this.product.idPic1 = response.pictures[0].id;
        } 
        if(response.pictures[1]) {
          this.product.picture2 = response.pictures[1].picture;
          this.product.idPic2 = response.pictures[1].id;
        }
        if(response.pictures[2]) {
          this.product.picture3 = response.pictures[2].picture;
          this.product.idPic3 = response.pictures[2].id;
        }
      }
    }, (error) => {
      //console.log(error)
    });
  }

  async presentModal(id?:any) {
    const modal = await this.modalController.create({
      component: ModalPresentationComponent,
      componentProps: {
        value: id,
        producto: this.parameter
      }
    });

    modal.onDidDismiss().then((data: any) => {
      this.getSingle(this.parameter)
    });

    return await modal.present();
  }

  async presentAlertConfirm(id:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar la presentación?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            //console.log('Confirm Okay');
            this.deletePresentacion(id)
          }
        }
      ]
    });

    await alert.present();
  }

  deletePresentacion(id:any) {
    this.thirdService.deleteType(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Presetación Eliminada', 'La presentación fue eliminada exitosamente.');
      this.getSingle(this.parameter);
    }, (error) => {
      console.clear
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar el producto?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            //console.log('Confirm Okay');
            this.delete(this.parameter)
          }
        }
      ]
    });

    await alert.present();
  }

  delete(id:any) {
    this.productoService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Producto Eliminado', 'El producto fue eliminado exitosamente.');
      this.modalController.dismiss(this.parameter);
    }, (error) => {
      console.clear
    });
  }

}
