import { Component, OnInit } from '@angular/core';
import { path } from 'src/app/config.module';
import { ProductoService } from 'src/app/_service/producto.service';
import { PresentacionService } from 'src/app/_service/presentacion.service';
import { ModalController, NavParams } from '@ionic/angular';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-modal-presentation',
  templateUrl: './modal-presentation.component.html',
  styleUrls: ['./modal-presentation.component.scss'],
})
export class ModalPresentationComponent implements OnInit {
  //PROPIEDADES
  public Table:any[] = [];
  public idUser:any = path.id;
  selectItem:any = 'beneficios';
  title:any;
  presentaciones:boolean = true;
  formulario:boolean = false;
  btnDisabled:boolean;
  parameter:any;
  data = {
    presentacion: '',
    product: '',
    cost: '',
    quantity: '',
    description: '',
    calificacion: 0,
    price: '',
    app: path.id,
    tipo: 0,
    state: 0,
  }

  //CONSTRUCTOR
  constructor(
    public mainService: PresentacionService,
    public secondService: ProductoService,
    public modalController: ModalController,
    public navParams: NavParams,
    public notificationService: NotificacionService
  ) {
    this.title = 'Presentaciones'
    this.parameter = this.navParams.get('value');
    if(this.parameter) {
      this.getSingle(this.parameter);
    } else {
      this.data.product = this.navParams.get('producto');
    }
  }

  ngOnInit() {
    this.getAll();
  }


  select(id:any, nombre:any) {
    this.presentaciones = false;
    this.formulario = true;
    this.data.presentacion = id;
    this.data.description = nombre;
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    this.presentaciones = false;
    this.formulario = true;
    this.mainService.getSingleType(parameter)
    .subscribe(response => {
      this.data = response;
    }, error => {
      //console.log(error)
    });
  }

  //CARGAR LOS RETOS
  public getAll() {
    this.mainService.getAll()
    .subscribe(response => {
      this.Table = []
      this.Table = response;
      //console.log(response)
    }, error => {
      console.clear
    })
  }

  //CERRAR MODAL
  public closeModal() {
    this.modalController.dismiss();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll();
      refresher.complete();
    }, 2000);
  }

  //SAVE CHANGES
  public saveChanges() {
    this.btnDisabled = true;
    //console.log(this.data)
    if(this.parameter) {
      this.update(this.data);
    } else {
      this.create(this.data);
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.createType(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Producto Agregado', 'El Producto fue agregado exitosamente.');
      this.modalController.dismiss(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.updateType(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Producto Actualizado', 'El Producto fue actualizado exitosamente.');
      this.modalController.dismiss(res);
      //console.log(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }
}
