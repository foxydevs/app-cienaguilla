import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { path } from 'src/app/config.module';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { OrdenService } from 'src/app/_service/orden.service';
import { AlertController, ModalController } from '@ionic/angular';
import { ModalMiCompraComponent } from './modal-mi-compra/modal-mi-compra.component';
import { VentaService } from 'src/app/_service/venta.service';

@Component({
  selector: 'app-mi-compra',
  templateUrl: './mi-compra.page.html',
  styleUrls: ['./mi-compra.page.scss'],
})
export class MiCompraPage implements OnInit {
  parameter:string;
  public idClient:any;
  public idApp:any = path.id;
  orders:any[];
  ventas:any[];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  moneda = localStorage.getItem('currentCurrency');

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private notificationService: NotificacionService,
    private ordersService: OrdenService,
    private ventasService: VentaService,
    private alertController: AlertController,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.parameter = this.activatedRoute.snapshot.paramMap.get('type');
    //this.getAll(+localStorage.getItem("currentId"));
    this.getAllVentas(+localStorage.getItem("currentId"));
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  /*//CARGAR ORDENES
  public getAll(id:any) {
    this.notificationService.alertLoading('Cargando...', 10000)
    this.ordersService.getByClients(id)
    .subscribe(response => {
      //console.log(response)
      this.orders = []
      this.orders = response;
      this.orders.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      console.clear
      this.notificationService.dismiss();
    })
  }*/

  //CARGAR ORDENES
  public getAllVentas(id:any) {
    this.notificationService.alertLoading('Cargando...', 10000)
    this.ventasService.getByClients(id)
    .subscribe(response => {
      console.log(response)
      this.ventas = []
      this.ventas = response;
      this.ventas.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      console.clear
      this.notificationService.dismiss();
    })
  }

  //VER DETALLES DE LA ORDEN
  /*public detailOrder(ern:any, token:any, id:any) {
    let parameter = {
      ern: ern,
      tokenID: token,
      id: id,
      type: 'detail'
    }
    let chooseModal = this.modal.create('DetailOrdersClientPage', { parameter });
    chooseModal.present();
  }*/

  async presentModal(id:any) {
    const modal = await this.modalController.create({
      component: ModalMiCompraComponent,
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      
    });

    return await modal.present();
  }

  async cancel(id:any) {
    let order = {
      opcion: "0",
      entrega: null,
      id: id
    }
    const alert = await this.alertController.create({
      header: 'Mi Pedido',
      message: '¿Deseas cancelar tu pedido?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            //console.log('Confirm Okay');
            this.notificationService.alertLoading('Cancelando...', 10000)
            this.ventasService.updateEstado(order)
            .subscribe(response => {                 
              //this.getAll(this.idClient);
              this.getAllVentas(+localStorage.getItem("currentId"));
              console.log(response)
              this.notificationService.dismiss();
              console.clear
            }, (error) => {
              this.notificationService.dismiss();
              console.clear
            });
          }
        }
      ]
    });

    await alert.present();
  }

  async accept(id:any) {
    let order = {
      state: '1',
      id: id
    }
    const alert = await this.alertController.create({
      header: 'Mi Pedido',
      message: '¿Su pedido a sido entregado?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            //console.log('Confirm Okay');
            this.notificationService.alertLoading('Cancelando...', 10000)
            this.ordersService.update(order)
            .subscribe(response => {                 
              //this.getAll(this.idClient);
              this.getAllVentas(+localStorage.getItem("currentId"));
              this.notificationService.dismiss();
              console.clear
            }, (error) => {
              this.notificationService.dismiss();
              console.clear
            });
          }
        }
      ]
    });

    await alert.present();
  }

  //PAGAR ORDEN
  /*public payProduct(o:any) {
    //console.log(o)
    let orden = {
      cantidad: o.quantity,
      descripcion: o.products.name,
      precio: o.unit_price,
      id: o.id,
      url: "http://me.gtechnology.gt",
      ern: o.ern,
      state: 4,
      applicacion: o.user
    }
    if(!o.ern) {
      orden.ern = this.generate(4)
    }
    this.parameter.ern = o.ern;
    this.ordersService.pagar(orden)
    .subscribe(res => {
      let url = res.token;
      let parameter = {
        ern: this.parameter.ern,
        tokenID: ''
      }
      //CARGAR ULTIMO LINK
      const browser = this.iab.create(url, '_blank', 'location=yes,clearsessioncache=yes,clearcache=yes');
      browser.on('loadstart').subscribe((e) => {
        this.url = e.url;
        //URL TOKEN NUEVOhttp://toktok.foxylabs.xyz/home/comprobante/ca4d0a9023b11b5600d1598868ed1827/02t778
        let tokenUrlComparation = e.url;
        //URL TOKEN A COMPARAR
        let token = this.url.replace('http://toktok.foxylabs.xyz/home/comprobante/','');
        let ernReplace = '/' + orden.ern;
        let tokenFinally = token.replace(ernReplace, '')
        parameter.tokenID = tokenFinally;
        let urlFinally = 'http://toktok.foxylabs.xyz/home/comprobante/' + tokenFinally + ernReplace;
        if(tokenUrlComparation == urlFinally) {
          if(tokenFinally.length >= 30) {
            this.navCtrl.setRoot(MyOrdersClientPage)
            browser.close()
            let chooseModal = this.modal.create('DetailOrdersClientPage', { parameter });
            chooseModal.present();
          }
        }
      });
      //CARGAR SALIDA
      browser.on('exit').subscribe((data) =>  {
        this.getAll(this.idClient);
      });
    }, (error) => {
      //console.log(error)
    })
  }*/

  public generate(longitude)
  {
    let i:number
    var caracteres = "123456789+-*abcdefghijkmnpqrtuvwxyz123456789+-*ABCDEFGHIJKLMNPQRTUVWXYZ12346789+-*";
    var password = "";
    for (i=0; i<longitude; i++) password += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
    return '02' + password;
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      //this.getAll(this.idClient);
      this.getAllVentas(+localStorage.getItem("currentId"));
      refresher.target.complete();
    }, 2000);
  }

}
