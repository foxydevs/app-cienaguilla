import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { OrdenService } from 'src/app/_service/orden.service';
import { VentaService } from 'src/app/_service/venta.service';
import { DireccionService } from 'src/app/_service/direccion.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-modal-mi-compra',
  templateUrl: './modal-mi-compra.component.html',
  styleUrls: ['./modal-mi-compra.component.scss'],
})
export class ModalMiCompraComponent implements OnInit {
  title:string;
  parameter:any;
  data:any;
  data2:any;
  total:number = 0;
  direccion:any;
  btnDisabled:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  moneda = localStorage.getItem('currentCurrency');
  userID = localStorage.getItem('currentId');
  administratorApp = localStorage.getItem('currentType');
  order = {
    entrega: null,
    opcion: "",
    id: 0
  }

  constructor(
    private modalController: ModalController,
    private ordenService: OrdenService,
    private ventaService: VentaService,
    private direccionService: DireccionService,
    private navParams: NavParams,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    this.getVenta(this.parameter);
    this.order.id = this.parameter;
  }

  closeModal() {
    this.modalController.dismiss();
  }

  getSingle(id:number) {
    this.ordenService.getSingle(id)
    .subscribe((res) => {
      console.log(res)
      this.data = res;
      if(res.venta) {
        this.getVenta(res.venta)
      }
      if(res.direccion) {
        this.getDireccion(res.direccion)
      }
      if(res.entrega) {
        this.order.entrega = res.entrega;
      }
    }, (error) => {
      //console.log(error)
    })
  }

  getVenta(id:number) {
    this.ventaService.getSingle(id)
    .subscribe((res) => {
      console.log(res)
      this.data2 = res;
      if(res.direccion) {
        this.getDireccion(res.direccion)
      }
      res.detalle.forEach(element => {
        //console.log(element)
        this.total = this.total + element.subtotal;
      });
    }, (error) => {
      //console.log(error)
    })
  }

  getDireccion(id:number) {
    this.direccionService.getSingle(id)
    .subscribe((res) => {
      //console.log(res)
      this.direccion = res;
    }, (error) => {
      //console.log(error)
    })
  }

  saveChanges() {
    if(this.order.entrega) {
      this.order.entrega = this.order.entrega.split("T")[0];
      this.order.opcion = "4";
      this.update(this.order);
    } else {
      this.notificationService.alertToast("La fecha es requerida.")
    } 
  }

  saveChanges2() {
    this.order.opcion = "5";
    this.update2(this.order);
  }

  update(data:any) {
    this.ventaService.updateEstado(data)
    .subscribe(response => {                 
      this.notificationService.alertMessage("Venta Enviada", "La venta ha sido enviada.");
      this.modalController.dismiss(response);
    }, (error) => {
      console.error(error)
    });
  }

  update2(data:any) {
    this.ventaService.updateEstado(data)
    .subscribe(response => {                 
      this.notificationService.alertMessage("Venta Entregada", "La venta ha sido entregada.");
      this.modalController.dismiss(response);
    }, (error) => {
      console.error(error)
    });
  }
}
