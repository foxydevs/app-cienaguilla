import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiCompraPage } from './mi-compra.page';

describe('MiCompraPage', () => {
  let component: MiCompraPage;
  let fixture: ComponentFixture<MiCompraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiCompraPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiCompraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
