import { Component, OnInit } from '@angular/core';
import { DireccionService } from 'src/app/_service/direccion.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalDireccionComponent } from './modal-direccion/modal-direccion.component';
import { ModalController, AlertController } from '@ionic/angular';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-direccion',
  templateUrl: './direccion.page.html',
  styleUrls: ['./direccion.page.scss'],
})
export class DireccionPage implements OnInit {
  table:any = [];
  clientId = +localStorage.getItem('currentId');

  constructor(
    private mainService: DireccionService,
    private router: Router,
    private location: Location,
    private modalController: ModalController,
    private notificationService: NotificacionService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.getAll();
  }
  
  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      this.table = [];
      this.table = res;
      //console.log(res);
    }, (error) => {
      //console.log(error);
    })
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: ModalDireccionComponent,
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      if(data.data) {
        this.getAll();
      }
    });

    return await modal.present();
  }

  async presentAlertConfirm(id:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar la dirección?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            //console.log('Confirm Okay');
            this.delete(id)
          }
        }
      ]
    });

    await alert.present();
  }

  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Dirección Eliminada', 'La dirección fue eliminada exitosamente.');
      this.getAll();
    }, (error) => {
      console.clear
    });
  }

}
