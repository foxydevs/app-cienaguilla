import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { DireccionService } from 'src/app/_service/direccion.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-modal-direccion',
  templateUrl: './modal-direccion.component.html',
  styleUrls: ['./modal-direccion.component.scss'],
})
export class ModalDireccionComponent implements OnInit {
  parameter:string;
  public data = {
    direccion: '',
    ciudad: '',
    numero: '',
    calle: '',
    observacion: '',
    app: 144,
    client: localStorage.getItem('currentId'),
    id: '',
    tipo: 0,
    state: 0
  }
  btnDisabled:boolean;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private mainService: DireccionService,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value')
    if(this.parameter) {
      this.getSingle(+this.parameter);
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

    //GUARDAR CAMBIOS
  saveChanges() {
    //console.log(this.data)
    if(this.data.direccion) {
      if(this.parameter) {
        this.btnDisabled = true;
        this.update(this.data);
      } else {
        this.btnDisabled = true;
        this.create(this.data);
      }
    } else {
      this.notificationService.alertToast('La dirección es requerida.');
    }
  }

  getSingle(id:number) {
    this.mainService.getSingle(id)
    .subscribe(res => {
      this.data = res;
    }, error => {
      //console.log(error)
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Dirección Agregada', 'La dirección fue agregada exitosamente.');
      this.modalController.dismiss(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Dirección Actualizada', 'La dirección fue actualizada exitosamente.');
      this.modalController.dismiss(res);
      //console.log(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

}
