import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.page.html',
  styleUrls: ['./carrito.page.scss'],
})
export class CarritoPage implements OnInit {
  cartProducts:any[] = [];
  btnDisabled:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  authentication:any = localStorage.getItem('currentAuthentication');

  constructor(
  public alertController: AlertController,
  private router: Router,
  private notificationService:NotificacionService,
  private location: Location) {
  }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }


  async deleteCar(e:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Desea eliminar el producto del carrito?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.cartProducts.splice(this.cartProducts.indexOf(e),1)
            localStorage.removeItem('cart');
            localStorage.setItem('cart', JSON.stringify(this.cartProducts))
            if(this.cartProducts.length == 0) {
              this.btnDisabled = true;
            } else {
              this.btnDisabled = false;
            }
          }
        }
      ]
    });

    await alert.present();
  }

  public addMore(e:any) {
    //console.log('ANTES', e)
    let cant = parseInt(e.cantidad) + 1;
    let sub = cant * e.price;
    sub.toFixed(2);
    let data = {
      cantidad: cant,
      cantidadTotal: e.cantidadTotal,
      name: e.name,
      subtotal: sub,
      price: e.price,
      precioVenta: e.precioVenta,
      precioClienteEs: e.precioClienteEs,
      precioDistribuidor: e.precioDistribuidor, 
      producto: e.producto,
      picture: e.picture,
      opcion1: e.opcion1,
      opcion2: e.opcion2,
      opcion3: e.opcion3,
      opcion4: e.opcion4,
      opcion5: e.opcion5,
      opcion6: e.opcion6,
      presentacion: e.presentacion,
      comment: e.comment,
    }
    //console.log('DESPUES', data);
    for (var x in this.cartProducts) {
      if (this.cartProducts[x] == e) {
        if(this.cartProducts[x].cantidadTotal > this.cartProducts[x].cantidad) {
          this.cartProducts[x] = data;
          //console.log("EDICION", this.cartProducts[x])
          localStorage.setItem('cart', JSON.stringify(this.cartProducts));
        } else {
          this.notificationService.alertToast('Ya no hay mas existencias D:');
        }
        break; //Stop this loop, we found it!
      }
    }
  }

  public removeProduct(e:any) {
    //console.log('ANTES', e)
    let cant = parseInt(e.cantidad) - 1;
    let sub = cant * e.price;
    sub.toFixed(2);
    let data = {
      cantidad: cant,
      name: e.name,
      subtotal: sub,
      price: e.price,
      precioVenta: e.precioVenta,
      cantidadTotal: e.cantidadTotal,
      precioClienteEs: e.precioClienteEs,
      precioDistribuidor: e.precioDistribuidor, 
      producto: e.producto,
      picture: e.picture,
      opcion1: e.opcion1,
      opcion2: e.opcion2,
      opcion3: e.opcion3,
      opcion4: e.opcion4,
      opcion5: e.opcion5,
      opcion6: e.opcion6,
      comment: e.comment,
      presentacion: e.presentacion,
    }
    //console.log('DESPUES', data);
    for (var x in this.cartProducts) {
      if (this.cartProducts[x] == e) {
        if(this.cartProducts[x].cantidad > 1) {
          this.cartProducts[x] = data;
          //console.log("EDICION", this.cartProducts[x])
          localStorage.setItem('cart', JSON.stringify(this.cartProducts));
        }        
        break; //Stop this loop, we found it!
      }
    }
  }

  //CONFIRMAR CARRITO
  public confirmateCart() {
      
  }

  ionViewWillEnter() {
    this.cartProducts = JSON.parse(localStorage.getItem('cart'));
    //console.log(this.cartProducts)
    if(this.cartProducts.length == 0) {
      this.btnDisabled = true;
    } else {
      this.btnDisabled = false;
    }
  }

  confirmarCarrito() {
    if(localStorage.getItem('currentId')) {
      this.goToRoute('compra');
    } else {
      this.notificationService.alertMessage('Iniciar Sesión', 'Debes iniciar sesión para continuar con el proceso.');
      this.goToRoute('tablogin/tablogin/login')
    }
    
  }

}
