import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { NavParams, ModalController } from '@ionic/angular';
import { InformacionService } from 'src/app/_service/informacion.service';

//JQUERY
declare var google;

@Component({
  selector: 'app-informacion-modal',
  templateUrl: './informacion-modal.component.html',
  styleUrls: ['./informacion-modal.component.scss'],
})
export class InformacionModalComponent implements OnInit {
  schedules:any = [];
  data:any;
  parameter:any;
  myStyles = {
    "background-image": ""
  }
  map: any;
  last_latitud:any;
  last_longitud:any;

  constructor(
    private mainService: UsuarioService,
    private secondService: InformacionService,
    private navParams: NavParams,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    if(this.parameter==1) {
      this.getSingle();
    } else if(this.parameter==2) {
      this.getSingle();
    } else if(this.parameter==3) {
      this.getSingle();
      this.getAll();
    }
  }

  public getSingle(){
    this.mainService.getSingle(144)
    .subscribe((res) => {
      ////console.log(res)
      this.data = res;
      this.myStyles["background-image"] = "url("+res.picture+")";
      this.getMap(res.last_latitud, res.last_longitud);
    }, (error) => {
      console.clear;
    })
  }

  public getAll(){
    this.secondService.getSchedulerByUser(144)
    .subscribe((res) => {
      this.schedules = [];
      this.schedules = res;
    }, (error) => {
      console.clear;
    })
  }

  //CARGAR MAPA ACTUALIZAR
  public getMap(lat:any, lon:any) {
  let latitude = lat;
  let longitude = lon;
  this.last_latitud = latitude.toString();
  this.last_longitud = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 15
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.last_latitud = evt.latLng.lat();
      this.last_longitud = evt.latLng.lng();
    });
  }

  closeModal() {
    this.modalController.dismiss();
  }

}
