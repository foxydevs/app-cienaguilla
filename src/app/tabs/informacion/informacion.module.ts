import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InformacionPage } from './informacion.page';
import { InformacionModalComponent } from './informacion-modal/informacion-modal.component';

const routes: Routes = [
  {
    path: '',
    component: InformacionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    InformacionPage,
    InformacionModalComponent
  ], entryComponents: [
    InformacionModalComponent
  ]
})
export class InformacionPageModule {}
