import { Component, OnInit } from '@angular/core';
import { CategoriaService } from 'src/app/_service/categoria.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalController, NavParams } from '@ionic/angular';
import { path } from 'src/app/config.module';

//JQUERY
declare var $:any;

@Component({
  selector: 'app-modal-categoria',
  templateUrl: './modal-categoria.component.html',
  styleUrls: ['./modal-categoria.component.scss'],
})
export class ModalCategoriaComponent implements OnInit {
  data = {
    name: '',
    description : '',
    user_created: path.id,
    estado: 1,
    state: 1,
    picture: 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatars/QtZbn4Z3sLd0QCK7WcUB56tm4pGo88qNpi88TNMl.png',
    id: '',
    parent: ''
  }
  btnDisabled:boolean;
  basePath:string = path.path;

  constructor(
    private categoriaService: CategoriaService,
    private notificationService: NotificacionService,
    private modalController: ModalController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    this.data.id = this.navParams.get('value');
    this.data.parent = this.navParams.get('parent');
    if(this.data.id) {
      this.getSingle(+this.data.id);
    }
  }

  saveChanges() {
    if(this.data.name) {
      if(this.data.description) {
        this.btnDisabled = true;
        if(this.data.id) {
          this.update(this.data);
        } else {
          this.create(this.data);
        }
      } else {
        this.notificationService.alertToast('La descripcion es requerida');
      }
    } else {
      this.notificationService.alertToast('El nombre es requerido');
    }
  }

  public create(data:any) {
    this.categoriaService.create(data)
    .subscribe((res)=> {
      this.notificationService.alertMessage('Categoría Agregada', 'La categoría ha sido agregada exitósamente.');
      this.modalController.dismiss(res);
    }, (err) => {
      console.error(err);
      this.btnDisabled = false;
      this.notificationService.alertToast('Ha ocurrido un error intente más tarde D:');
    });
  }

  public update(data:any) {
    this.categoriaService.update(data)
    .subscribe((res)=> {
      this.notificationService.alertMessage('Categoría Actualizada', 'La categoría ha sido actualizada exitósamente.');
      this.modalController.dismiss(res);
    }, (err) => {
      console.error(err);
      this.btnDisabled = false;
      this.notificationService.alertToast('Ha ocurrido un error intente más tarde D:');
    });
  }

  getSingle(id:number) {
    this.categoriaService.getSingle(id)
    .subscribe((res) => {
      //console.log(res)
      this.data = res;
      if(!res.picture) {
        this.data.picture = 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatars/QtZbn4Z3sLd0QCK7WcUB56tm4pGo88qNpi88TNMl.png'
      }
    }, (error) => {
      //console.log(error)
    })
  }
  
  closeModal() {
    this.modalController.dismiss();
  }

  //SUBIR IMAGENES
  uploadImage(archivo:any, id:any) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}categorys/upload/${this.data.id}`;

    var size=archivos[0].size;
    var type=archivos[0].type;

    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0]
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.picture)
            $("#"+id).val('')
          }
        );
      } else {
        this.notificationService.alertToast('La imagen es demasiado grande.')
      }
    } else {
      this.notificationService.alertToast('El tipo de imagen no es válido.')
    }
  }

}
