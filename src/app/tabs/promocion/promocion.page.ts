import { Component, OnInit } from '@angular/core';
import { PromocionService } from 'src/app/_service/promocion.service';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalController, AlertController, ActionSheetController } from '@ionic/angular';
import { ModalPromocionComponent } from './modal-promocion/modal-promocion.component';
import { DescuentoService } from 'src/app/_service/descuento.service';
import { ModalDescuentoComponent } from './modal-descuento/modal-descuento.component';
import { ProductoService } from 'src/app/_service/producto.service';

@Component({
  selector: 'app-promocion',
  templateUrl: './promocion.page.html',
  styleUrls: ['./promocion.page.scss'],
})
export class PromocionPage implements OnInit {
  public promociones:any[] = [];
  public descuentos:any[] = [];
  selectItem:any = 'promocion';
  administratorApp:string;

  constructor(
    private mainService: DescuentoService,
    private secondService: PromocionService,
    private clipboard: Clipboard,
    private notificationService: NotificacionService,
    private modalController: ModalController,
    private alertController: AlertController,
    private actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.administratorApp = localStorage.getItem('currentType')
    this.getAllPromocion();
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
    ////console.log('Segment changed', ev);
    if(ev.detail.value == 'promocion') {
      this.getAllPromocion();
    } else if(ev.detail.value == 'descuento') {
      this.getAllDescuento();
    }
  }

  public getAllPromocion() {
    this.secondService.getByUser(144)
    .subscribe(res => {
      console.log(res)
      this.promociones = []
      this.promociones = res;
    }, (error) => {
      ////console.log(error)
    })
  }

  public getAllDescuento() {
    this.mainService.getByUser(144)
    .subscribe(res => {
      ////console.log(res)
      this.descuentos = []
      this.descuentos = res;
    }, (error) => {
      ////console.log(error)
    })
  }

  public copy(item:any) {
    if(this.administratorApp == 'Administrador') {
      this.presentActionSheet(item.id);
    } else {
      this.clipboard.copy(item.codigo);
      this.notificationService.alertToast('Codigo copiado al portapapeles.');
    }
  }

  public getPromocion(item:any) {
    if(this.administratorApp == 'Administrador') {
      this.presentActionSheet2(item.id);
    } else {
      this.presentModal2(item.id);
    }
  }

  async presentModal(id?:any) {
    const modal = await this.modalController.create({
      component: ModalPromocionComponent,
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      this.getAllDescuento();
    });

    return await modal.present();
  }

  async presentModal2(id?:any) {
    const modal = await this.modalController.create({
      component: ModalDescuentoComponent,
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      this.getAllPromocion();
    });

    return await modal.present();
  }

  async presentAlertConfirm(id:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar el descuento?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            //console.log('Confirm Okay');
            this.delete(id)
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlertConfirm2(id:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar la promoción?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            //console.log('Confirm Okay');
            this.delete2(id)
          }
        }
      ]
    });

    await alert.present();
  }

  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Descuento Eliminada', 'La descuento fue eliminada exitosamente.');
      this.getAllDescuento();
    }, (error) => {
      console.clear
    });
  }

  delete2(id:any) {
    this.secondService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Promocion Eliminada', 'La promocion fue eliminada exitosamente.');
      this.getAllPromocion();
    }, (error) => {
      console.clear
    });
  }

  async presentActionSheet(id:number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      buttons: [
      {
        text: 'Actualizar',
        icon: 'create',
        handler: () => {
          //console.log('Share clicked');
          this.presentModal(id);
        }
      },
      {
        text: 'Eliminar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          //console.log('Delete clicked');
          this.presentAlertConfirm(id);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async presentActionSheet2(id:number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Albums',
      buttons: [
      {
        text: 'Actualizar',
        icon: 'create',
        handler: () => {
          //console.log('Share clicked');
          this.presentModal2(id);
        }
      },
      {
        text: 'Eliminar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          //console.log('Delete clicked');
          this.presentAlertConfirm2(id);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }


}
