import { Component, OnInit } from '@angular/core';
import { PromocionService } from 'src/app/_service/promocion.service';
import { ProductoService } from 'src/app/_service/producto.service';
import { NavParams, ModalController } from '@ionic/angular';
import { path } from 'src/app/config.module';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalDescuentoDetalleComponent } from '../modal-descuento-detalle/modal-descuento-detalle.component';

//JQUERY
declare var $:any;

@Component({
  selector: 'app-modal-descuento',
  templateUrl: './modal-descuento.component.html',
  styleUrls: ['./modal-descuento.component.scss'],
})
export class ModalDescuentoComponent implements OnInit {
  //PROPIEDADES
  public parameter:any;
  public title:any;
  public product:any;
  public btnDisabled:boolean;
  public table:any[] = [];
  public basePath:string = path.path;
  administratorApp:string;
  selectItem:any = 'actualizar';
  selectItemProduct:any;
  public data = {
    title: '',
    description: '',
    inicio: '',
    fin: '',
    descuento: '',
    product: 'null',
    event: 'null',
    state: 0,
    user_created: path.id,
    picture: localStorage.getItem('currentPicture'),
    id: ''
  }

  //CONSTRUCTOR
  constructor(
    public mainService: PromocionService,
    public secondService: ProductoService,
    public thirdService: ProductoService,
    public navParams: NavParams,
    public notificationService: NotificacionService,
    public modalController: ModalController
  ) {
    this.parameter = this.navParams.get('value')
    this.administratorApp = localStorage.getItem('currentType')
    this.getAll(path.id);
    if(this.parameter) {
      this.title = "Edición Promoción";
      this.getSingle(this.parameter);
    } else {
      this.title = "Agregar Promoción";
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

  ngOnInit() {}

  //CARGAR BENEFICIOS
  public getAll(id:any) {
    this.secondService.getByUser(id)
    .subscribe(res => {
      this.table = []
      this.table = res;
    }, error => {
      console.log(error)
    })
  }

  //GET SINGLE
  public getSingleProduct(id:any) {
    this.thirdService.getSingle(id)
    .subscribe(res => {
      this.product = res;
      console.log(res)
    }, (error) => {
      ////console.log(error)
    })
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.product == 'null') {
      this.data.product = null;
    }
    if(this.data.event == 'null') {
      this.data.event = null;
    }

    this.data.inicio = this.data.inicio.split("T")[0];
    this.data.fin = this.data.fin.split("T")[0];

    this.data.picture = $('img[alt="Avatar"]').attr('src');
    if(this.data.title) {
      if(this.data.description) {
        this.btnDisabled = true;
        if(this.parameter) {
          this.update(this.data)
        } else {
          this.create(this.data)
        }
      } else {
        this.notificationService.alertToast('La descripción es requerida.');
      }
    } else {
      this.notificationService.alertToast('El titulo es requerido.');
    }
  }


  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Promoción Agregado', 'La promoción fue agregada exitosamente.');
      this.data = res;
      this.parameter = res.id;
      this.btnDisabled = false;
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Promoción Actualizado', 'El promoción fue actualizada exitosamente.');
      this.modalController.dismiss(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //OBTENER DATOS
  getSingle(id:any) {
    this.mainService.getSingle(id)
    .subscribe(res => {
      console.log(res)
      this.data = res;
      console.log(res.product)
      if(res.product) {
        this.getSingleProduct(res.product)
      }
    }, error => {
      console.log(error)
    })
  }

  //IMAGEN DE CATEGORIA
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'promotions'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.notificationService.alertToast('La imagen es demasiado grande.')
      }
    } else {
      this.notificationService.alertToast('El tipo de imagen no es válido.')
    }
  }

  saveChanges2 = (product:any) => {
    
    console.log(product);
    if(this.selectItemProduct.quantity > 0) {
      if(this.selectItemProduct) {
        let data = {
          cantidad: 1,
          cantidadTotal: this.selectItemProduct.quantity,
          name: product.name,
          subtotal: this.selectItemProduct.price-(this.selectItemProduct.price*(+this.data.descuento/100)),
          precioClienteEs: null,
          precioDistribuidor: null, 
          producto: product.id,
          picture: product.picture,
          price: this.selectItemProduct.price-(this.selectItemProduct.price*(+this.data.descuento/100)),
          precioVenta: this.selectItemProduct.price-(this.selectItemProduct.price*(+this.data.descuento/100)),
          presentacion: this.selectItemProduct.id,
          opcion1: this.selectItemProduct.id,
          opcion2: this.selectItemProduct.description,
          opcion3: null,
          opcion4: null,
          opcion5: null,
          opcion6: null,
          state: 4,
          comment: null
        }
        let cart:any[] = []
        cart = JSON.parse(localStorage.getItem('cart'))
        cart.push(data)
        localStorage.removeItem('cart')
        localStorage.setItem('cart', JSON.stringify(cart));
        this.notificationService.alertToast(data.name + ' agregado al carrito :D');
        this.selectItemProduct = "";
      }
    } else {
      this.notificationService.alertToast('Ya no hay existencias D:');
    }
  }

  //AÑADIR AL CARRITO
  saveChanges3 = (product:any) =>  {
    console.log(product)
    if(product.quantity > 0) {
      let data = {
        cantidad: 1,
        cantidadTotal: product.quantity,
        name: product.name,
        subtotal: product.price-(product.price*(+this.data.descuento/100)),
        price: product.price-(product.price*(+this.data.descuento/100)),
        precioVenta: product.price-(product.price*(+this.data.descuento/100)),
        precioClienteEs: null,
        precioDistribuidor: null, 
        presentacion: null,
        producto: product.id,
        picture: product.picture,
        opcion1: null,
        opcion2: null,
        opcion3: null,
        opcion4: null,
        opcion5: null,
        opcion6: null,
        state: 4,
        comment: null
      }
      //console.log(data)
      let cart:any[] = []
      cart = JSON.parse(localStorage.getItem('cart'))
      cart.push(data)
      localStorage.removeItem('cart')
      localStorage.setItem('cart', JSON.stringify(cart));
      this.notificationService.alertToast(data.name + ' agregado al carrito :D');
    } else {
      this.notificationService.alertToast('Ya no hay existencias D:');
    }
  }

  async presentModal(id?:any) {
    const modal = await this.modalController.create({
      component: ModalDescuentoDetalleComponent,
      componentProps: {
        value: id,
        categoria: this.parameter
      }
    });
    return await modal.present();
  }

}
