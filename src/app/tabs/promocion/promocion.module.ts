import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PromocionPage } from './promocion.page';
import { ModalPromocionComponent } from './modal-promocion/modal-promocion.component';
import { ModalDescuentoComponent } from './modal-descuento/modal-descuento.component';
import { ModalDescuentoDetalleComponent } from './modal-descuento-detalle/modal-descuento-detalle.component';

const routes: Routes = [
  {
    path: '',
    component: PromocionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    PromocionPage,
    ModalPromocionComponent,
    ModalDescuentoComponent,
    ModalDescuentoDetalleComponent
  ], entryComponents: [
    ModalPromocionComponent,
    ModalDescuentoComponent,
    ModalDescuentoDetalleComponent
  ]
})
export class PromocionPageModule {}
