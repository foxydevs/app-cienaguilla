import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { DescuentoService } from 'src/app/_service/descuento.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { path } from 'src/app/config.module';

@Component({
  selector: 'app-modal-promocion',
  templateUrl: './modal-promocion.component.html',
  styleUrls: ['./modal-promocion.component.scss'],
})
export class ModalPromocionComponent implements OnInit {
  parameter:string;
  data = {
    id: '',
    cantidad: '',
    app: path.id,
    description: '',
    codigo: '',
    picture: 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatar/SRKESWmUTyo7J9TMYg1An26mDb7CpWQF6eU6pFV6.png',
    latitude: '',
    usado: 0,
    state: 0,
    longitude: 0
  }
  btnDisabled:boolean;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private mainService: DescuentoService,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value')
    this.data.codigo = this.generate(10);
    if(this.parameter) {
      this.getSingle(+this.parameter);
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

    //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.cantidad) {
      if(this.data.description) {
        if(this.parameter) {
          this.btnDisabled = true;
          this.update(this.data);
        } else {
          this.btnDisabled = true;
          this.create(this.data);
        }
      } else {
        this.notificationService.alertToast('La descripción es requerido.');
      }
    } else {
      this.notificationService.alertToast('La cantidad es requerida.');
    }
  }

  getSingle(id:number) {
    this.mainService.getSingle(id)
    .subscribe(res => {
      this.data = res;
    }, error => {
      console.log(error)
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Descuento Agregado', 'El Descuento fue agregado exitosamente.');
      this.modalController.dismiss(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Descuento Actualizado', 'El Descuento fue actualizado exitosamente.');
      this.modalController.dismiss(res);
      console.log(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  public generate(longitude)
  {
    let i:number
    var caracteres = "1234567890abcdefghijkmnpqrtuvwxyz0123456789ABCDEFGHIJKLMNPQRTUVWXYZ123467890";
    var password = "";
    for (i=0; i<longitude; i++) password += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
    return password;
  }

}
