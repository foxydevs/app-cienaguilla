import { Component, OnInit } from '@angular/core';
import { PresentacionService } from 'src/app/_service/presentacion.service';
import { ModalController, AlertController } from '@ionic/angular';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ModalPresentacionComponent } from './modal-presentacion/modal-presentacion.component';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-presentacion',
  templateUrl: './presentacion.page.html',
  styleUrls: ['./presentacion.page.scss'],
})
export class PresentacionPage implements OnInit {
  table:any[];
  search:string;

  constructor(
    private mainService: PresentacionService,
    private modalController: ModalController,
    private alertController: AlertController,
    private notificationService: NotificacionService,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
    this.getAll();
  }

  closeModal() {
    this.modalController.dismiss();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      this.table = [];
      this.table = res;
      //console.log(res);
    }, (error) => {
      //console.log(error);
    })
  }

  async presentAlertConfirm(id:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar la presentación?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            //console.log('Confirm Okay');
            this.delete(id)
          }
        }
      ]
    });

    await alert.present();
  }

  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Presetanción Eliminada', 'La presentación fue eliminada exitosamente.');
      this.getAll();
    }, (error) => {
      console.clear
    });
  }

  async presentModal(id?:number) {
    const modal = await this.modalController.create({
      component: ModalPresentacionComponent,
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      if(data.data) {
        this.getAll();
      }
    });

    return await modal.present();
  }
}
