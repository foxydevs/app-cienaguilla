import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { PresentacionService } from 'src/app/_service/presentacion.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { path } from 'src/app/config.module';

@Component({
  selector: 'app-modal-presentacion',
  templateUrl: './modal-presentacion.component.html',
  styleUrls: ['./modal-presentacion.component.scss'],
})
export class ModalPresentacionComponent implements OnInit {
  parameter:string;
  public data = {
    name: '',
    description: '',
    video: '',
    user_created: path.id,
    app: path.id,
    picture: 'https://bpresentacion.s3.us-west-2.amazonaws.com/avatars/QtZbn4Z3sLd0QCK7WcUB56tm4pGo88qNpi88TNMl.png',
    id: ''
  }
  btnDisabled:boolean;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private mainService: PresentacionService,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value')
    if(this.parameter) {
      this.getSingle(+this.parameter);
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

  //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.name) {
      if(this.data.description) {
        this.btnDisabled = true;
        if(this.parameter) {
          this.update(this.data);
        } else {
          this.create(this.data);
        }
      } else {
        this.notificationService.alertToast('La dirección es requerida.');
      }
    } else {
      this.notificationService.alertToast('El nombre es requerido.');
    }
  }

  getSingle(id:number) {
    this.mainService.getSingle(id)
    .subscribe(res => {
      this.data = res;
    }, error => {
      //console.log(error)
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Presentación Agregada', 'La presentación fue agregada exitosamente.');
      this.modalController.dismiss(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Presentación Actualizada', 'La presentación fue actualizada exitosamente.');
      this.modalController.dismiss(res);
      //console.log(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

}
