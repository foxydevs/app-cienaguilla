import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError} from 'rxjs/operators';
import { path } from 'src/app/config.module';

@Injectable({
  providedIn: 'root'
})
export class OrdenService {
  private basePath:string = path.path;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  constructor(private http: HttpClient) { }

  //HANDLE ERROR
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('Un error ha ocurrido:', error.error.message);
    } else {
      console.error(
      `Backend returned code ${error.status}, ` +
      `body was: `, error.error);
    }
    return throwError(
      error);
  };

  //GET ALL
  getAll() : Observable<any> {
    let url = `${this.basePath}orders`;
    return this.http.get(url, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    );
  }

  //GET SINGLE
  getSingle(id:number) : Observable<any> {
    let url = `${this.basePath}orders/${id}`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //GET SINGLE
  getByUser(id:number) : Observable<any> {
    let url = `${this.basePath}users/${id}/orders`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //GET SINGLE
  getByProducts(id:number) : Observable<any> {
    let url = `${this.basePath}products/${id}/orders`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //CREATE
  public create(data:any) : Observable<any> {
    let url = `${this.basePath}orders`;
    return this.http.post(url, data, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  //CREATE
  public pagar(data:any) : Observable<any> {
    let url = `${this.basePath}orders/1/pagar`;
    return this.http.post(url, data, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  //CREATE
  public pagarQPP(data:any) : Observable<any> {
    let url = `${this.basePath}orders/1/pagarqpp`;
    return this.http.post(url, data, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  //CREATE
  public update(data:any) : Observable<any> {
    let url = `${this.basePath}orders/${data.id}`;
    return this.http.put(url, data, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  //DELETE
  delete(id:number) : Observable<any> {
    let url = `${this.basePath}orders/${id}`;
    return this.http.delete(url, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    );
  }

  //GET SINGLE
  getOrdersByUserState(id:number, state:number) : Observable<any> {
    let url = `${this.basePath}users/${id}/orders/${state}`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //GET SINGLE
  getByClients(id:number) : Observable<any> {
    let url = `${this.basePath}clients/${id}/orders`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //GET SINGLE
  findSingle(ern:number) : Observable<any> {
    let url = `${this.basePath}orders/find/${ern}`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  //GET SINGLE
  transaction(id:any,token:any) : Observable<any> {
    let url = `${this.basePath}orders/${id}/comprobante?token=${token}`;
    return this.http.get(url)
    .pipe(
      catchError(this.handleError)
    );
  }

}
