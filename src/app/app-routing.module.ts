import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './_guard/auth.guard';
import { HomeGuard } from './_guard/home.guard';

const routes: Routes = [
  {
    path: 'tablogin',
    canActivate: [HomeGuard],
    loadChildren: () => import('./tabs-login/tabs-login.module').then(m => m.TabsLoginPageModule)
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule'},
  { path: 'subcategoria/:id', loadChildren: './tabs/subcategoria/subcategoria.module#SubcategoriaPageModule' },
  { path: 'producto/:id', loadChildren: './tabs/producto/producto.module#ProductoPageModule' },
  { path: 'carrito', loadChildren: './tabs/carrito/carrito.module#CarritoPageModule' },
  { path: 'direccion', loadChildren: './tabs/direccion/direccion.module#DireccionPageModule' },
  { path: 'compra', loadChildren: './tabs/compra/compra.module#CompraPageModule' },
  { path: 'mi-compra/:type', loadChildren: './tabs/mi-compra/mi-compra.module#MiCompraPageModule' },
  { path: 'presentacion', loadChildren: './tabs/presentacion/presentacion.module#PresentacionPageModule' },
  { path: 'pedido', loadChildren: './tabs/pedido/pedido.module#PedidoPageModule' },
  { path: 'usuario', loadChildren: './tabs/usuario/usuario.module#UsuarioPageModule' },
  { path: 'cliente', loadChildren: './tabs/tabs.module#TabsPageModule', canActivate: [AuthGuard] },
  {
    path: '',
    redirectTo: 'cliente',
    pathMatch: 'full'
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
