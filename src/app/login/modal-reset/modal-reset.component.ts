import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/_service/auth.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-modal-reset',
  templateUrl: './modal-reset.component.html',
  styleUrls: ['./modal-reset.component.scss'],
})
export class ModalResetComponent implements OnInit {
  title:string = 'Recuperar Contraseña';
  passwordType:string = 'password';
  passwordShow:boolean = false;
  passwordType2:string = 'password';
  passwordShow2:boolean = false;
  btnDisabled:boolean = false;

  data = {
    username: ''
  }

  constructor(
    private usuarioService: AuthService,
    private notificationService: NotificacionService,
    private modalController: ModalController
    ) { }

  ngOnInit() {}

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

  //TOGGLE PASSWORD
  togglePassword2() {
    if(this.passwordShow2) {
      this.passwordShow2 = false;
      this.passwordType2 = 'password';
    } else {
      this.passwordShow2 = true;
      this.passwordType2 = 'text';
    }
  }

  saveChanges() {
    if(this.data.username) {
      this.btnDisabled = true;
      this.create();
    } else {
      this.notificationService.alertToast('El usuario es requerido.');
    }
  }

  public create() {
    this.usuarioService.changePassword(this.data)
    .subscribe((res)=> {
      this.notificationService.alertMessage('Contraseña Recuperada', 'Te hemos enviado un correo a: ' + res.email);
      this.closeModal();
    }, (err) => {
      console.error(err);
      this.btnDisabled = false;
      this.notificationService.alertToast('Usuario no encontrado.');
    });
  }

}
