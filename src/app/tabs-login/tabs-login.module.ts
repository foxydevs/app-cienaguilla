import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TabsLoginPage } from './tabs-login.page';

const routes: Routes = [
  {
    path: 'tablogin',
    component: TabsLoginPage,
    children: [
      {
        path: 'categoria',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./../tabs/categoria/categoria.module').then(m => m.CategoriaPageModule)
          }
        ]
      },
      {
        path: 'promocion',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./../tabs/promocion/promocion.module').then(m => m.PromocionPageModule)
          }
        ]
      },
      {
        path: 'informacion',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./../tabs/informacion/informacion.module').then(m => m.InformacionPageModule)
          }
        ]
      },
      {
        path: 'login',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./../login/login.module').then(m => m.LoginPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tablogin/tablogin/categoria',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tablogin/tablogin/categoria',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsLoginPage]
})
export class TabsLoginPageModule {}
