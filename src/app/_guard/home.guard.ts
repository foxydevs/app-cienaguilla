import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class HomeGuard implements CanActivate {

    constructor(
      private router: Router,
      public navCtrl: NavController
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (localStorage.getItem('currentUser')) {
        this.navCtrl.navigateForward('tablogin/tablogin/login');
      }

      if (!localStorage.getItem('currentUser')) {
        return true;
      }
    }
}

